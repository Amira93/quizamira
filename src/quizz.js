let quiz=document.getElementById("quiz");
let question= document.getElementById("question");
let opt1=document.getElementById("option1");
let opt2=document.getElementById("option2");
let opt3=document.getElementById("option3");
let opt4=document.getElementById("option4");
let resultat=document.getElementById("result");

let nextbutton= document.getElementById("next");
let quitter=document.getElementById('quit');

let Tquestion=questions.length; 
let score=0;
let quesindex=0;


let count=90;
let counter=setInterval(timer, 1000); 
/**
  * @returns 
 */
function timer()
{
  count=count-1;
  if (count < 0)
  {
   	quit();
     return;
  }

 document.getElementById("timer").innerHTML=count + " s"; 
}

nextbutton.addEventListener("click",next_question);
/**
 * 
 * @param {number} quesindex 
 * @returns 
 */
function donner_question(quesindex) 
{
	question.textContent=quesindex+1+") "+questions[quesindex][0];
	opt1.textContent=questions[quesindex][1];
	opt2.textContent=questions[quesindex][2];
	opt3.textContent=questions[quesindex][3];
	opt4.textContent=questions[quesindex][4];
	 return;
};
donner_question(0);

/**
 * 
 * @returns 
 */
function next_question()
{
	let select_rep= document.querySelector('input:checked');
	if(!select_rep)
		{alert("Veuillez sélectionner une réponse");return;}

	if(select_rep.value==questions[quesindex][5])
		{
			score=score+1;
		}
		select_rep.checked=false;
	     quesindex++;
	     if(quesindex==Tquestion-1)
	     	nextbutton.textContent="Finish";
	     if(quesindex==Tquestion)
	     {
	         quit();
		     return;
	     }
		 
        donner_question(quesindex);
}
const img = document.createElement('img')
 img.src = ""

 /**
  * 
  */
function quit()
{         
	      quiz.style.display='none';
		  quitter.style.display="none";
          resultat.style.display='';
		
		  if (score === 0)
		  {
			  img.src = "image/1.png";
		  } else
		  if (score >= 5)
		  {
			img.src = "image/5.png";
		  }else
		  {
			  img.src = "image/3.png";
		  }		 
		  count = 0;
		 resultat.innerHTML="Votre score est : <br>  "+score+" sur "+Tquestion+ '<br>';
		 resultat.appendChild(img);
         
}
