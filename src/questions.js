let questions = [
  ["A l’intérieur de quel tag HTML peut-on mettre du Javascript?", "<Javascript>", "<scripting>", "<script>", "Aucune de ces réponses n'est correcte", "3"],
  ["Quelle est la bonne syntaxe pour utiliser un fichier javascript externe 'monfichier.js'", " script href = 'monfichier.js'", " script name = 'monfichier.js'", " script src = 'monfichier.js'","Aucune de ces réponses n'est correcte","3"],
  ["Quelle propriété donne le nombre de caracteres d’une string (chaîne de caractère?", "Numbers", "length", " Num","Aucune de ces réponses n'est correcte","2"],
  ["Si A = true et B = false, quelle valeur sera égale à true?", "  A && B", "  A || B", " A & B","Aucune de ces réponses n'est correcte","2"],
  ["Comment insérer un commentaire de plusieurs lignes en Javascript?", "  <!-- ceci est un commentaire sur plusieurs lignes-->", "   // ceci est un commentaire sur plusieurs lignes//", "  /* ceci est un commentaire sur plusieurs lignes */","Aucune de ces réponses n'est correcte","2"],
  [" Quelle est la bonne syntaxe du 'if' en Javascript?", "if (i === 3)", "  if i===3", " if i=3","Aucune de ces réponses n'est correcte","1"],
  [" Comment écrire une condition 'if' pour vérifier si « a » n’est pas égal à 2?", "if a <> 2", "  if (a != 2)", "if a =! 2 then","Aucune de ces réponses n'est correcte","2"],
  ["Comment exécuter la fonction maFonction avec a et b comme arguments?", " maFonction()", " maFonction(a,b)", " maFonction(a;b)","Aucune de ces réponses n'est correcte","2"],
  ["Quel code écrira dans la console exactement 5 fois 'Hello' ?", "for (let i = 0; i<= 5; i++) {console.log('Hello')}", "  for (let i = 1; i< 5; i++) {console.log('Hello')}", " for (let i = 0; i< 5; i++) {console.log('Hello')}","Aucune de ces réponses n'est correcte","3"],
  ["Quelle est la bonne syntaxe pour créer un array en Javascript?", "let myArray = (1:'Pomme', 2:'Banane', 3:'Kiwi')", "let myArray = ['Pomme', 'Banane', 'Kiwi']", "let myArray = {'Pomme', 'Banane', 'Kiwi'}","Aucune de ces réponses n'est correcte","2"],
  ];