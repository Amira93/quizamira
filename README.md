## presentation de projet quiz
Projet ‘’Quiz’’
Date : 17 mai 2021
Réalisateur : Amira Habi

Description : 

Ce projet entre dans le cadre de la formation ‘Développeur Web’ de l’école Simplon.co Lyon. Le concept de cette formation est de suivre des cours théoriques et des ateliers pratiques puis de les valider par des projets personnels. 
Le projet ‘Quiz’ est notre deuxième projet après le projet ‘Portfolio’. 
Ce projet consiste a réaliser une application web de type Quizz/QCM/questionnaire pour se familiariser avec les concepts vus dans les cours et ateliers en utilisant HTML, CSS, Bootstrap et JavaScript.

Le principe de ce projet est simple. Il consiste à proposer un certain nombre de questions où l’utilisateur doit choisir une réponse parmi des propositions affichées. L’utilisateur a le choix de parcourir les questions et de répondre d’une manière séquentielle en cliquant sur le bouton Suivant ou de quitter le quiz en cliquant sur le bouton Quitter. Chaque réponse a un score. Pour des raisons de simplicité, le choix a été fixé comme suit : une réponse correcte a un score de 1 et une réponse incorrecte a un score de 0. A la fin du Quiz, l’utilisateur aura une note finale qui représente la somme de tous ses scores.


## la maquette 
![wireframe quizz](public/image/maquette1.png)
![wireframe quizz](public/image/maquette2.png)

## le lien de gitlab:
https://amira93.gitlab.io/quizamira
